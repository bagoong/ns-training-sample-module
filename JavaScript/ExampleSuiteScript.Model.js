define('ExampleSuiteScript.Model'
, [
    'Backbone'

  , 'Utils'
  ]
, function
  (
    Backbone

  , Utils
  )
{
  'use strict';

  return Backbone.Model.extend({
    urlRoot: Utils.getAbsoluteUrl('services/ExampleSuiteScript.Service.ss')
  })
});