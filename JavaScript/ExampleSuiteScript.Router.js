define('ExampleSuiteScript.Router'
, [
    'Backbone'

  , 'ExampleSuiteScript.View'
  , 'ExampleSuiteScript.Model'
  ]
, function
  (
    Backbone

  , View
  , Model
  )
{
  'use strict';

  return Backbone.Router.extend ({
    routes:
    {
      'examplesuitescript': 'loadTheModel'
    }

  , initialize: function (application)
    {
      this.application = application;
    }

  , loadTheModel: function ()
    {
      var model = new Model()
        , self = this;

      model.fetch().done(function ()
      {
        var view = new View
        ({
          application: self.application
        , model: model
        });

        view.showContent();
      });
    }
  })
});