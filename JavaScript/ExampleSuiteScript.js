define('ExampleSuiteScript'
, [
    'ExampleSuiteScript.Router'

  , 'SC.Configuration'
  ]
, function
  (
    Router

  , Configuration
  )
{
  'use strict';

  if (Configuration.exampleSuiteScript.enabled)
  {
    return {
      mountToApp: function (application)
      {
        return new Router(application)
      }
    }
  }
});